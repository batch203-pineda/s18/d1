// console.log("Hello Wolrd");

// We learned how to gather data from the user input using prompt();

function printInfo () {
    let nickname = prompt ("Enter your nickname");
    console.log("Hi, " +nickname);
}

// printInfo();







function printName(name){
    console.log("My name is " +name);
}

printName("Juana");
printName("John");

/* 
    [SECTION] Parameters and Arguments

    Parameter
    -   "name" is called a parameter
    -   A "parameter" acts as a named variable/container that exists only inside the function
    -   It is used to store information that is provided to a function when it is called/invoke.

    Argument
    -   "Juana" and "John", the information/data provided derectly into the function is called "argument".
    -   Values passed when invoking a function are called arguments.
    -   These arguments are then stored as the parameter within the function.

    Variables can also be passed as an argument
*/

    let sampleVariable = "Yui";

    printName(sampleVariable);
    printName(); // function calling without arguments will result to undefined parameters.


    // Check if a specific number is divisible by 8

    function checkDivisibilityBy8(num){
        let remainder = num % 8;
        console.log("The remainder of " + num + " devided b 8 is: " + remainder);
        let isDivisibleBy8 = remainder === 0;
        console.log("Is " + num + " divisible by 8?");
        console.log(isDivisibleBy8);
    }

    checkDivisibilityBy8(64);
    checkDivisibilityBy8(28);





/* 
    [SECTION] Function as argument

    Function parameters can also accept other functions as arguments.

    Some complex functions uses other functions as arguments to perform more complicated result.
*/

    function argumentFunction(){
        console.log("This function was passed as an argument before the message was printed.");
    }

    function invokeFunction(argumentFunction) {
        argumentFunction();
    }

    // A function used without a parenthesis is normally associated with using the function as an argument to another function.
    invokeFunction(argumentFunction);

    console.log(argumentFunction);

/* 
    [SECTION] Using Multiple Parameters

    Multiple "arguments" will corresponds to the number of "parameters" declared in a function in succeeding order.
*/

    function createFullName(firstName, middleName, lastName) {
        console.log("My full name is " + firstName + " " + middleName + " " +lastName);
    }
                // firstName middleName lastName
    createFullName("Joel", "Pangilinan", "Pineda");

    // In Javascript providing more/less arguments than expected parameters will not return an error.
    // In other programming languages, this will return an error stating that "the expected number of argument do not match the number of parameters".
    createFullName("Joel", "Pineda");
    createFullName("Joel", "Pangilinan", "Pineda", "Hello");

    // Using variables as arguments
    let firstName = "John";
    let middleName = "Smith";
    let lastName = "Doe";

    createFullName(firstName, middleName, lastName);







/*  [SECTION] The return statement

    The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called.

*/

function returnFullName(firstName, middleName, lastName) {
    /*  You can also create a variable inside the function 
    let fullName = firstName + " " + middleName + " " +lastName;
    return fullName;
 */
    return firstName + " " + middleName + " " + lastName;
    // This line of code will not be printed.
    console.log("This is printed inside the function");
}

// Value returned from the function is stored in our variable.
let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);
// Using return statement value, we can further use/manipulate in our program instead of only printing / displaying in our console log.
console.log("My name is " +completeName);
console.log(returnFullName("Joel", "Pangilinan", "Pineda"));



// when a function only have console.log() it result will return

function printPlayerInfo(userName, level, job) {
    console.log("Username: " + userName);
    console.log("Level: " + level);
    console.log("Job: " + job);  
}

let user1 = printInfo("knight_white", 95, "paladin");
console.log(user1); // returns undefined.